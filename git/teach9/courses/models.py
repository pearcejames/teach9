"""
Definition of models.
"""
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.utils import timezone
import random

# Create your models here.

class Course(models.Model):
	owner = models.ForeignKey(User, related_name="user_owner", on_delete=models.CASCADE)
	name = models.CharField(max_length = 100)
	slug = models.SlugField(unique = True)
	content = models.TextField()
	start_date = models.DateField(auto_now = False, auto_now_add = False)
	finish_date = models.DateField(auto_now = False, auto_now_add = False)
	attendees = models.ManyToManyField(User, related_name="user_attendee", blank=True)
	deleted = models.BooleanField(default = False)
	delete_date = models.DateField(auto_now = False, auto_now_add = False, null=True, blank=True)

	def __unicode__(self):
		return self.name
	def __str__(self):
		return self.name
	def get_absolute_url(self):
		return reverse("courses:course_detail", kwargs = {"course": self.slug})
	def get_edit_url(self):
		return reverse("courses:course_update", kwargs = {"course": self.slug})
	def get_delete_url(self):
		return reverse("courses:course_delete", kwargs = {"course": self.slug})

	def create_lesson_url(self):
		return reverse("courses:lesson_create", kwargs = {"course": self.slug})
	def get_lessons(self):
		return Lesson.objects.filter(course = self)
	def create_assignment_url(self):
		return reverse("courses:assignment_create", kwargs = {"course": self.slug})
	
class Lesson(models.Model):
	name = models.CharField(max_length = 100)
	content = models.TextField()
	summary = models.CharField(max_length = 200)
	course = models.ForeignKey(Course, on_delete=models.CASCADE)
	slug = models.SlugField(unique = True)
	lesson_date = models.DateField(auto_now = False, auto_now_add = False, null=True, blank=True)
	visible = models.BooleanField(default = True)

	def __unicode__(self):
		return self.name
	def __str__(self):
		return self.name
	def get_absolute_url(self):
		return reverse("courses:lesson_detail", kwargs = {"course":self.course.slug, "lesson":self.slug})
	def get_edit_url(self):
		return reverse("courses:lesson_update", kwargs = {"course":self.course.slug, "lesson":self.slug})
	def get_delete_url(self):
		return reverse("courses:lesson_delete", kwargs = {"course": self.course.slug, "lesson":self.slug})

	def create_activity_url(self):
		return reverse("courses:activity_create", kwargs = {"course":self.course.slug, "lesson":self.slug})
	def get_activities(self):
		return Activity.Objects.filter(lesson = self)


class Assignment(models.Model):
	name = models.CharField(max_length = 100)
	content = models.TextField(blank=True)
	notes = models.TextField(blank=True)
	owner = models.ForeignKey(User, on_delete=models.CASCADE)
	course = models.ForeignKey(Course, on_delete=models.CASCADE)
	lessons = models.ManyToManyField(Lesson, blank=True)
	slug = models.SlugField(unique = True)
	approved = models.BooleanField(default = False)

	def __unicode__(self):
		return self.name
	def __str__(self):
		return self.name
	def get_absolute_url(self):
		return reverse("courses:assignment_detail", kwargs = {"course":self.course.slug, "assignment":self.slug})
	def get_edit_url(self):
		return reverse("courses:assignment_update", kwargs = {"course":self.course.slug, "assignment":self.slug})
	def get_delete_url(self):
		return reverse("courses:assignment_delete", kwargs = {"course": self.course.slug, "assignment":self.slug})


class Activity(models.Model):
	name = models.CharField(max_length = 100)
	content = models.TextField()
	lesson = models.ForeignKey(Lesson, blank=True, on_delete=models.CASCADE)
	votable = models.BooleanField(default = False)
	votes = models.ManyToManyField(User, blank = True)
	visible = models.BooleanField(default = True)
	slug = models.SlugField(unique = True)

	def __unicode__(self):
		return self.name
	def __str__(self):
		return self.name
	def get_absolute_url(self):
		return reverse("courses:activity_detail", kwargs = {"course":self.lesson.course.slug, "lesson":self.lesson.slug, "activity":self.slug})
	def get_edit_url(self):
		return reverse("courses:activity_update", kwargs = {"course":self.lesson.course.slug, "lesson":self.lesson.slug, "activity":self.slug})
	def get_delete_url(self):
		return reverse("courses:activity_delete", kwargs = {"course": self.lesson.course.slug, "lesson":self.lesson.slug, "activity":self.slug})
	def get_votes(self):
		return self.votes.all()
	def vote_url(self):
		return reverse("courses:activity_vote", kwargs = {"course": self.lesson.course.slug, "lesson":self.lesson.slug, "activity":self.slug})



def pre_save_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance, sender)

def create_slug(instance, sender, new_slug = None):
	slug = slugify(instance.name) + str(random.randint(0,9)) + str(random.randint(0,9)) #prevents creation of slugs that are already used in urls, like create
	if new_slug is not None:
		slug = new_slug
	qs = sender.objects.filter(slug = slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug(instance, sender, new_slug = new_slug)
	return slug

pre_save.connect(pre_save_receiver, sender = Course)
pre_save.connect(pre_save_receiver, sender = Lesson)
pre_save.connect(pre_save_receiver, sender = Assignment)
pre_save.connect(pre_save_receiver, sender = Activity)