"""
Definition of urls for Teach9.
"""

from datetime import datetime
from django.conf.urls import include, url
import django.contrib.auth.views
from django.conf import settings
from django.conf.urls.static import static

import app.forms
import app.views

application_title = settings.COURSE_SITE_TITLE
# Uncomment the next lines to enable the admin:
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^login/$',
        django.contrib.auth.views.login,
        {
            'template_name': 'app/login.html',
            'authentication_form': app.forms.BootstrapAuthenticationForm,
            'extra_context':
            {
				'application':application_title,
                'title': 'Log in',
                'year': datetime.now().year,
            }
        },
        name='login'),
    url(r'^logout$',
        django.contrib.auth.views.logout,
        {
            'next_page': '/',
        },
        name='logout'),

    url(r'^admin/', include(admin.site.urls)),

	url(r'^courses/', include("courses.urls", namespace="courses")),

	url(r'^profile/$', app.views.profile, name="profile"),
	url(r'^profile/create/$', app.views.create_profile, name="create_profile"),
	url(r'^profile/edit/$', app.views.edit_profile, name="edit_profile"),
	url(r'^profile/editpass/$', django.contrib.auth.views.password_change, name="edit_password"),
	url(r'^profile/$', app.views.profile, name='password_change_done'),

]