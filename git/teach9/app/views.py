"""
Definition of views.
"""

from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.template import RequestContext
from django.contrib import messages
from datetime import datetime
from .forms import CreateProfileForm, EditProfileForm
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
from django.http.response import Http404
from django.conf import settings

application_title = settings.COURSE_SITE_TITLE

def home(request):
	"""Renders the home page."""
	assert isinstance(request, HttpRequest)
	context = {
		'title': "Home",
		'application':application_title,

	}
	return render(request, 'app/index.html',context)

def profile(request):
	"""Renders the profile page."""
	if not request.user.is_authenticated:
		raise Http404
	context = {
	'title': request.user.username + "Profile",
	'application':application_title,
	'profile_edit_url':reverse("edit_profile"),
	'password_edit_url':reverse("edit_password"),
	}
	return render(request, 'app/profile.html', context)

def create_profile(request):
	"""Renders the create profile form"""
	form = CreateProfileForm( request.POST or None)
	if form.is_valid():
		new_profile = form.save(commit=False)
		new_profile.is_staff = False
		new_profile.is_superuser = False
		new_profile.set_password(form.cleaned_data['password'])
		new_profile.save()
		return HttpResponseRedirect(reverse("home"))
	context = {
	'title': "Create Profile",
	'application': application_title,
	'form': form,
	}
	return render(request, 'app/create_profile.html', context)

def edit_profile(request):
	"""Renders the create profile form"""
	form = EditProfileForm(request.POST or None, request.FILES or None, instance=request.user)
	if form.is_valid and request.POST:
		request.user = form.save(commit=False)
		request.user.save()
		return HttpResponseRedirect(reverse("profile"))
	context = {
	'form': form,
	'title': "Change Password",
	'application': application_title,
	}
	return render(request, 'app/edit_profile.html', context)