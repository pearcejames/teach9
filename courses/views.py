"""
Definition of views.
"""

from urllib import quote_plus
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.http.response import Http404
from django.contrib import messages
from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from datetime import datetime
from .forms import CreateCourseForm, EditCourseForm, CreateLessonForm, EditLessonForm, AdminEditCourseForm, CreateActivityForm, EditActivityForm, TeacherCreateAssignmentForm, TeacherEditAssignmentForm, CreateAssignmentForm, EditAssignmentForm
from .models import Course, Lesson, Activity, Assignment

application_title = settings.COURSE_SITE_TITLE # Name of the application, will be displayed on the navbar


#region Assignments
def assignment_create(request, course=None): 
	course_object = get_object_or_404(Course, slug=course)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_attending(request.user, course_object): #See if user is logged in and see if they are attending the course
		raise Http404 #Raise 404 error if the user is not attending or the teacher of the course 

	if course_object.owner == request.user:
		form = TeacherCreateAssignmentForm(request.POST or None)
		form.fields['lessons'].queryset = Lesson.objects.filter(course=course_object)
		#For the teacher give a different form
	else:
		form = CreateAssignmentForm(request.POST or None)
		form.fields['lessons'].queryset = Lesson.objects.filter(course=course_object).filter(visible=True)
		#Students form
	if form.is_valid():
		assignment_object = form.save(commit=False)
		assignment_object.course = course_object
		assignment_object.owner = request.user
		assignment_object.save()
		form.save_m2m()
		return HttpResponseRedirect(course_object.get_absolute_url())
		#Saving the assignment
	context = {
		"title": "Create Assignment",
		'application':application_title,
		"form": form,
		"course": course_object,
		"assignment_list_url": reverse("courses:assignment_list", kwargs={"course": course_object.slug})
		}
	return render(request, "assignment_create.html", context)

def assignment_list(request, course=None):
	"""Renders the assignment list"""
	course_object = get_object_or_404(Course, slug=course)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_attending(request.user, course_object): #See if user is logged in and see if they are attending the course
		raise Http404 #Raise 404 error if the user is not attending or the teacher of the course 
	if authenticate_owner(request.user, course_object):
		permission = True
		my_assignments = Assignment.objects.filter(course=course_object).filter(owner=request.user)
		staff_assignments = Assignment.objects.filter(course=course_object).exclude(owner=request.user)
		#For the teacher give these assignments and "permission"
	else:
		permission = False
		staff_assignments = None
		my_assignments = Assignment.objects.filter(course=course_object).filter(owner=request.user)
		#For the students give these assignments and not "permission"
	context = {
		"title": "Assignments",
		'application':application_title,
		"course": course_object,
		"my_assignments": my_assignments,
		"staff_assignments": staff_assignments,
		"permission": permission,
		"assignment_list_url": reverse("courses:assignment_list", kwargs={"course": course_object.slug})
		}

	return render(request, "assignment_list.html", context)

def assignment_detail(request, course=None, assignment=None):
	"""Renders the assignment detail page"""
	course_object = get_object_or_404(Course, slug=course)
	assignment_object = get_object_or_404(Assignment, slug=assignment)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if authenticate_owner(request.user, course_object) or authenticate_owner(request.user, assignment_object): # If the user is not the owner of the course or the assignment raise 404
		context = {
			"title": assignment_object.name,
			"application":application_title,
			"course": course_object,
			"assignment": assignment_object,
			"assignment_list_url": reverse("courses:assignment_list", kwargs={"course": course_object.slug})
			}
		return render(request, "assignment_detail.html", context)

	else: raise Http404

def assignment_update(request, course=None, assignment=None):
	course_object = get_object_or_404(Course, slug=course)
	assignment_object = get_object_or_404(Assignment, slug=assignment)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if authenticate_owner(request.user, course_object) or authenticate_owner(request.user, assignment_object): # If the user is not the owner of the course or the assignment raise 404

		if authenticate_owner(request.user, course_object):
			form = TeacherCreateAssignmentForm(request.POST or None, instance=assignment_object)
			form.fields['lessons'].queryset = Lesson.objects.filter(course=course_object)
			#Give this form to the teacher
		else:
			form = CreateAssignmentForm(request.POST or None, instance=assignment_object)
			form.fields['lessons'].queryset = Lesson.objects.filter(course=course_object).filter(visible=True)
			#Give this form to the student
		if form.is_valid():
			assignment_object = form.save(commit=False)
			assignment_object.save()
			form.save_m2m()
			return HttpResponseRedirect(assignment_object.get_absolute_url())
			#Saving the assignment
		context = {
			"title": "Create Assignment",
			'application':application_title,
			"form": form,
			"course": course_object,
			"assignment": assignment_object,
			"assignment_list_url": reverse("courses:assignment_list", kwargs={"course": course_object.slug})
			}
		return render(request, "assignment_edit.html", context)
	else: raise Http404

def assignment_delete(request, course=None, assignment=None):
	course_object = get_object_or_404(Course, slug=course)
	assigment_object = get_object_or_404(Assignment, slug=assignment)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if authenticate_owner(request.user, course_object) or authenticate_owner(request.user, assignment_object): # If the user is not the owner of the course or the assignment raise 404
		assignment_object.delete()
		return redirect("courses:assignment_list", course=course)
		#Allows the teacher to delete students assignments
	raise Http404
#endregion

#region Activitis
def activity_create(request, course=None, lesson=None):
	course_object = get_object_or_404(Course, slug=course)
	lesson_object = get_object_or_404(Lesson, slug=lesson)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_owner(request.user, course_object):
		raise Http404
		#If the user is not the owner of the course or an admin raise 404
	form = CreateActivityForm(request.POST or None)
	if form.is_valid():
		activity = form.save(commit=False)
		activity.lesson = lesson_object
		activity.save()
		return HttpResponseRedirect(activity.get_absolute_url())
		#Save the activity
	context = {
		"title": "Create Activity",
		"lesson": lesson_object,
		'application':application_title,
		"form": form,
		}
	return render(request, "activity_create.html", context)

def activity_detail(request, course=None, lesson=None, activity=None):
	"""Renders the activity detail page"""
	course_object = get_object_or_404(Course, slug=course)
	lesson_object = get_object_or_404(Lesson, slug=lesson)
	activity_object = get_object_or_404(Activity, slug = activity)
	if course_object.deleted and not request.user.is_superuser:
		raise Http404
	if not authenticate_attending(request.user, course_object):
		raise Http404
	if request.user.is_superuser or request.user == course_object.owner:#If the course has been "deleted" raise 404
		permission = True
	else:
		permission = False

	if request.user in activity_object.get_votes():
		voted=True
	else: voted=False
	context = {
		"title": activity_object.name,
		"permission": permission,
		'application':application_title,
		"activity": activity_object,
		"voted": voted,
}
	return render(request, "activity_detail.html", context)


def activity_vote(request, course=None, lesson=None, activity=None):
	"""Handles voting for activities"""
	course_object = get_object_or_404(Course, slug=course)
	lesson_object = get_object_or_404(Lesson, slug=lesson)
	activity_object = get_object_or_404(Activity, slug = activity)
	if course_object.deleted:
		return HttpResponseRedirect(activity_object.get_absolute_url())
	if not authenticate_attending(request.user, course_object):
		raise Http404
	if request.user == course_object.owner:#If the course has been "deleted" redirect to the activity_object
		return HttpResponseRedirect(activity_object.get_absolute_url())
	
	if request.user in activity_object.get_votes(): 
		activity_object.votes.remove(request.user)
	else:
		activity_object.votes.add(request.user)

	return HttpResponseRedirect(activity_object.get_absolute_url())



def activity_update(request, course=None,lesson=None, activity=None):
	"""Renders the edit/update activity page"""
	course_object = get_object_or_404(Course, slug=course)
	activity_object = get_object_or_404(Activity, slug=activity)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_owner(request.user, course_object): #If the user is not course owner or admin raise 404
		raise Http404

	form = EditActivityForm(request.POST or None, request.FILES or None, instance = activity_object)
	if form.is_valid():
		activity_object = form.save(commit=False)
		activity_object.save()
		form.save_m2m()
		return HttpResponseRedirect(activity_object.get_absolute_url())
		#Saving the activity and redirecting to its detail page
	context = {
		"title": activity_object.name,
		"form": form,
		"activity":activity_object,
		'application':application_title,
	}
	return render(request,"activity_edit.html", context)

def activity_delete(request, course=None,lesson=None, activity=None):
	course_object = get_object_or_404(Course, slug=course)
	activity_object = get_object_or_404(Activity, slug=activity)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_owner(request.user, course_object): #If not the course owner raise 404
		raise Http404
	activity_object.delete() #delete the activity
	return redirect("courses:lesson_detail", course=course, lesson=lesson)

#endregion

	#region Lessons

def lesson_create(request, course=None):
	""""Renders the create lesson page"""
	course_object = get_object_or_404(Course, slug=course)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_owner(request.user, course_object):
		raise Http404
	form = CreateLessonForm( request.POST or None)
	if form.is_valid():
		lesson = form.save(commit=False)
		lesson.course = course_object
		lesson.save()
		return HttpResponseRedirect(course_object.get_absolute_url())

	context = {
		"title": "Create Lesson",
		'application':application_title,
		"form": form,
		"instance":course_object,
		"course": course_object,
		}
	return render(request, "lesson_create.html", context)

def lesson_detail(request, course=None, lesson=None):
	"""Renders the lesson detail page """
	course_object = get_object_or_404(Course, slug=course)
	lesson_object = get_object_or_404(Lesson, slug=lesson)
	if not authenticate_attending(request.user, course_object):
		raise Http404
	if authenticate_owner(request.user, course_object):
		permission=True
		activity_list = Activity.objects.filter(lesson = lesson_object)
	else:
		permission=False
		activity_list = Activity.objects.filter(lesson = lesson_object).filter(visible=True)
	context = {
		"title": lesson_object.name,
		'application':application_title,
		"lesson": lesson_object,
		"course":course_object,
		"activity_list": activity_list,
		"assignment_list": Assignment.objects.all().filter(lessons = lesson_object).filter(approved=True),
		"permission":permission,

	}
	return render(request, "lesson_detail.html", context)

def lesson_update(request, course=None, lesson=None):
	"""Renders the edit/update lesson page"""
	authenticated(request.user)
	course_object = get_object_or_404(Course, slug=course)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_owner(request.user, course_object):
		raise Http404
	lesson_object = get_object_or_404(Lesson, slug=lesson)
	form = EditLessonForm(request.POST or None, request.FILES or None, instance = lesson_object)
	if form.is_valid():
		lesson_object = form.save(commit=False)
		lesson_object.save()
		return HttpResponseRedirect(lesson_object.get_absolute_url())
	context = {
		"title": lesson_object.name,
		"form": form,
		"lesson":lesson_object,
		'application':application_title,
	}
	return render(request,"lesson_edit.html", context)

def lesson_delete(request, course=None, lesson=None):
	"""Delets the lesson if a superuser or owner of the course wants to"""
	course_object = get_object_or_404(Course, slug=course)
	lesson_object = get_object_or_404(Lesson, slug=lesson)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_owner(request.user, course_object): #If user is not owner or admin raise 404
		raise Http404

	lesson_object.delete() #delete the lesson
	return redirect("courses:course_detail", course=course)


#endregion

	#region Courses

def course_list(request):
	"""Renders the courses list"""
	if authenticate_staff(request.user):
		permission = True
		if request.user.is_superuser:
			admin_courses = Course.objects.all()
			attending_list = None
			my_courses_list = None
		else:
			admin_courses = None
			my_courses_list = Course.objects.filter(owner=request.user).exclude(deleted=True)
			attending_list = Course.objects.filter(attendees=request.user).exclude(deleted=True)
	else:
		permission = False
		admin_courses = None
		my_courses_list = None
		attending_list = Course.objects.filter(attendees=request.user).exclude(deleted=True)

	context = {
		"title": "Courses",
		'application':application_title,
		"course_create_url": reverse("courses:course_create"),
		"admin_courses": admin_courses,
		"course_list": attending_list,
		"my_courses": my_courses_list,
		"permission": permission,
		}


	return render(request,"course_list.html", context)

def course_create(request):
	"""Renders the create course page"""
	if not authenticate_staff(request.user):
		raise Http404
	form = CreateCourseForm( request.POST or None, request.FILES or None)
	form.fields['attendees'].queryset = User.objects.exclude(is_superuser=True).exclude(username=request.user)
	if form.is_valid():
		course_object = form.save(commit=False)
		course_object.owner = request.user
		course_object.save()
		form.save_m2m()
		return HttpResponseRedirect(course_object.get_absolute_url())
	context = {
		"form": form,
		"title": "Create Course",
		'application':application_title,
		"course_list_url": reverse("courses:course_list")
		}
	return render(request,"course_create.html", context)

def course_detail(request, course=None):
	"""Renders the course detail page"""
	course_object = get_object_or_404(Course, slug=course)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_attending(request.user, course_object): #If user is not attending course raise 404
		raise Http404
	if request.user.is_superuser or request.user == course_object.owner: #See if user has permission to edit/create objects
		permission = True
	else:
		permission = False
	context = {
		"title": course_object.name,
		"permission": permission,
		'application':application_title,
		"attendees": course_object.attendees.all(),
		"course": course_object,
		"assignment_list_url": reverse("courses:assignment_list", kwargs = {"course": course_object.slug}),
		"lessons": course_object.get_lessons(),
}
	return render(request, "course_detail.html", context)
	

def course_update(request, course=None):
	"""Renders the course edit/update page"""
	course_object = get_object_or_404(Course, slug=course)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if not authenticate_owner(request.user, course_object):
		raise Http404
	if request.user.is_superuser:
		form = AdminEditCourseForm(request.POST or None, request.FILES or None, instance = course_object)
	else:
		form = EditCourseForm( request.POST or None, request.FILES or None, instance = course_object)
		form.fields['attendees'].queryset = User.objects.exclude(is_superuser=True).exclude(username=course_object.owner.username)
	if form.is_valid():
		course_object = form.save(commit=False)
		course_object.save()
		form.save_m2m()
		return HttpResponseRedirect(course_object.get_absolute_url())
	context = {
		"title": course_object.name,
		"form": form,
		"course":course_object,
		'application':application_title,
		}
	return render(request,"course_edit.html", context)

def course_delete(request, course=None):
	"""Hides the course, if it was deleted by the owner, if it was deleted by a superuser deletes the course"""
	authenticated(request.user)
	course_object = get_object_or_404(Course, slug=course)
	if course_object.deleted and not request.user.is_superuser: #If the course has been "deleted" raise 404
		raise Http404
	if authenticate_owner(request.user, course_object):
		course_object.deleted = True
		course_object.delete_date = datetime.now()
		course_object.save()
	else:
		raise Http404
	if request.user.is_superuser:
		course_object.delete()	
	return redirect("courses:course_list")
	

#endregion

def authenticated(user): #returns True if the user is authenticated, else should raise 404
	if user.is_authenticated:
		return True
	else: raise Http404
	return False

def authenticate_staff(user): #Makes sure the user is authenticated and returns True if user is staff or superuser else returns False
	if authenticated(user):
		if user.is_staff or user.is_superuser:
			return True
	return False

def authenticate_owner(user, object): #Makes sure the user is authenticated and returns True if user is owner or superuser else returns False
	if authenticated(user):
		if object.owner == user or user.is_superuser:
			return True
	return False

def authenticate_attending(user, object):#Makes sure the user is authenticated and returns True if user is attending or owner course or superuser else returns False
	if authenticated(user):
		if authenticate_owner(user, object) or user in object.attendees.all():
			return True
	return False
