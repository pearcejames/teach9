"""
Definition of forms.
"""


from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from .models import Course, Lesson, Activity, Assignment
from functools import partial


DateInput = partial(forms.DateInput, {"class": "datepicker"})

class CreateAssignmentForm(forms.ModelForm):
	class Meta:
		model = Assignment
		fields = [
			"name",
			"content",
			"lessons",
			]
		widgets = { "lessons": forms.Select(), }
		labels = {
			"lessons": _("Lesson")
			}
		help_texts = {
			"name": _("Name of the Assignment, please follow your teachers naming conventions"),
			"content": _("Here you give you assignment answer and/or give links to the source of your answer"),
			"lessons": _("Select the lesson this assignment is tied to"),
			}

class EditAssignmentForm(forms.ModelForm):
	class Meta:
		model = Assignment
		fields = [
			"name",
			"content",
			"lessons",
			]
		widgets = {"lessons": forms.Select(), }
		labels = {
			"lessons": _("Lesson")
			}
		help_text = {
			"name": _("Name of the Assignment, please follow your teachers naming conventions"),
			"content": _("Here you give you assignment answer and/or give links to the source of your answer"),
			"lessons": _("Select the lesson this assignment is tied to"),
			}

class TeacherCreateAssignmentForm(forms.ModelForm):
	class Meta:
		model = Assignment
		fields = [
			"name",
			"content",
			"notes",
			"lessons",
			"approved",
			]
		widgets = {'lessons': forms.CheckboxSelectMultiple(), }
		labels = {
			"approved": _("Visible to attendees "),
			}
		help_texts = {
			"name": _("Name of the assignment"),
			"content": _("Main information about the assignment"),
			"notes": _("Write additional information about the assignment separate from the main content"),
			"lessons": _("Select the lessons where this assignment is visible."),
			"approved": _("Tick to make the assignment visible on the selected lessons."),
			}

class TeacherEditAssignmentForm(forms.ModelForm):
	class Meta:
		model = Assignment
		fields = [
			"name",
			"content",
			"notes",
			"lessons",
			"approved",
			]
		widgets = {'lessons': forms.CheckboxSelectMultiple(), }
		labels = {
			"approved": _("Visible to attendees "),
			}
		help_text = {
			"name": _("Name of the assignment"),
			"content": _("Main information about the assignment. If reviewing a students assignment give feedback in the notes section"),
			"notes": _("Write additional information about the assignment separate from the main content."),
			"approved": _("Tick to make the assignment visible to attendees of the course."),
			}


class TeacherReviewAssignmentForm(forms.ModelForm):
	class Meta:
		model = Assignment
		fields = [
			"name",
			"content",
			"notes",
			"approved",
			]
		widgets = {'lessons': forms.CheckboxSelectMultiple(), }
		labels = {

			}
		help_text = {
			"name": _("Name of the assignment"),
			"content": _("Main information about the assignment. Students will not be informed of changes made to their work."),
			"notes": _("Give a student feedback about their assignment here"),
			"approved": _("Tick to approve the students assignment as done."),
			}

class CreateActivityForm(forms.ModelForm):
	class Meta:
		model = Activity
		fields = [
			"name",
			"content",
			"votable",
			"visible",
			]
		widgets = { }
		labels = {

						
			}
		help_texts = {
			"name": _("Name of the activity"),
			"content":_("Main information about the activity"),
			"votable": _("Will the students be able to vote for this activity"),
			"visible": _("Can Attendees see this activity"),
			}

class EditActivityForm(forms.ModelForm):
	class Meta:
		model = Activity
		fields = [
			"name",
			"content",
			"votable",
			"visible",
			]
		widgets = {'lessons': forms.CheckboxSelectMultiple(),}
		labels = {

						
			}
		help_texts = {
			"name": _("Name of the activity"),
			"content":_("Main information about the activity"),
			"votable": _("Will the students be able to vote for this activity"),
			"visible": _("Can Attendees see this activity"),
			}

class CreateLessonForm(forms.ModelForm):
	class Meta:
		model = Lesson
		fields = [
			"name",
			"content",
			"summary",
			"lesson_date",
			"visible",
			]
		widgets = {'lesson_date': DateInput(),}
		labels = {

			}
		help_texts = {
			"name": _("Name of the lesson"),
			"content": _("Details of what will be done at/for the lesson"),
			"summary": _("A short summary of what will be done at/for the lesson"),
			"lesson_date": _("Optional date of the lesson"),
			"visible": _("Can Attendees see this lesson"),
			}

class EditLessonForm(forms.ModelForm):
	class Meta:
		model = Lesson
		fields = [
			"name",
			"content",
			"summary",
			"lesson_date",
			"visible",
			]
		widgets = {'lesson_date': DateInput(),}
		labels = {
			

			}
		help_texts = {
			"name": _("Name of the lesson"),
			"content": _("Details of what will be done at/for the lesson"),
			"summary": _("A short summary of what will be done at/for the lesson"),
			"lesson_date": _("Optional date of the lesson"),
			"visible": _("Can Attendees see this lesson"),
			}


class CreateCourseForm(forms.ModelForm):	
	class Meta:
		model = Course
		fields = [
			"name", 
			"content", 
			"start_date",
			"finish_date",
			"attendees",
			]
		widgets = {'start_date': DateInput(), 'finish_date': DateInput(),'attendees': forms.CheckboxSelectMultiple(),}
		labels = {


			}
		help_texts = {
			"name": _("Name of the course"),
			"content": _("Main information about the course"),
			"start_date": _("Starting date of the course"),
			"finish_date": _("Ending date of the course"),
			"attendees": _("Select the students from the list who will be attending the course"),
			}

class EditCourseForm(forms.ModelForm):	
	class Meta:
		model = Course
		fields = [
			"name", 
			"content", 
			"start_date",
			"finish_date",
			"attendees",
			]
		widgets = {'start_date': DateInput(), 'finish_date': DateInput(), 'attendees': forms.CheckboxSelectMultiple(),}
		labels = {

						
			}
		help_texts = {
			"name": _("Name of the course"),
			"content": _("Main information about the course"),
			"start_date": _("Starting date of the course"),
			"finish_date": _("Ending date of the course"),
			"attendees": _("Select the students from the list who will be attending the course"),
			}

class AdminEditCourseForm(forms.ModelForm):	
	class Meta:
		model = Course
		fields = [
			"owner",
			"name", 
			"content", 
			"start_date",
			"finish_date",
			"attendees",
			"deleted",
			"delete_date",
			]
		widgets = { 'start_date': DateInput(), 'finish_date': DateInput(),'delete_date': DateInput(), 'attendees': forms.CheckboxSelectMultiple(),}
		labels = {

			}
		help_texts = {
			"name": _("Name of the course"),
			"content": _("Main information about the course"),
			"start_date": _("Starting date of the course"),
			"finish_date": _("Ending date of the course"),
			"attendees": _("Select the students from the list who will be attending the course"),
			"deleted": _("Wether the course seems deleted, uncheck to restore course"),
			"delete_date": _("Date when the course was deleted by the course owner"),
			}
